### Intenationalization

Commento is, first of all, multilanguage. You can:

* Force you embed or script to be in one of the supported langs
* Let it detect the user language declared by the browser (default)

We aim to initially provide english, spanish, and portuguese, then french, chinese, japanese, deutsch and dutch.

If you host your own instance, you can get to support any language you want, just by setting them in /config/config.js, as well as changing the locales path, like this:

    localization: {
      locales: ["en", "es", "fr", "de", "ja", "pt"],
      directory: "./config/locales"
    }

We use an npm module called just "i18n", which is documented here:
[https://www.npmjs.com/package/i18n]



