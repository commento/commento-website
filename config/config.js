var path = require('path'),
  rootPath = path.normalize(__dirname + '/..'),
  env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'commento'
    },
    port: process.env.PORT || 3001,
    db: process.env.MONGOLAB_URI || 'mongodb://localhost/commento-development',
    localization: {
      locales: ["en", "es", "fr", "de", "ja", "pt"],
      defaultLocale: 'es',
      directory: "./config/locales"
    }
  },

  test: {
    root: rootPath,
    app: {
      name: 'commento'
    },
    port: process.env.PORT,
    db: process.env.MONGOLAB_URI,
    localization: {
      locales: ["en", "es", "fr", "de", "ja", "pt"],
      directory: "./config/locales"
    }
  },

  production: {
    root: rootPath,
    app: {
      name: 'commento'
    },
    port: process.env.PORT,
    db: process.env.MONGOLAB_URI,
    localization: {
      locales: ["en", "es", "fr", "de", "ja", "pt"],
      directory: "./config/locales"
    }
  }
};

module.exports = config[env];