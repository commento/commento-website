var debug = require('debug'),
  express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  Comment = mongoose.model('Comment'),
  login = require('./middlewares/login')

module.exports = function(app, server) {
  app.use('/', router);

};

router.get('/',
  login.redirectIfAuth('/user'),
  function(req, res, next) {
    Comment.find(function(err, comments) {
      if (err) return next(err);
      res.render('index', {
        title: 'Commento.io',
        comments: comments.reverse(),
        env: {
          AUTH0_CLIENT_ID: process.env.AUTH0_CLIENT_ID,
          AUTH0_DOMAIN: process.env.AUTH0_DOMAIN,
          AUTH0_CALLBACK_URL: process.env.AUTH0_CALLBACK_URL || 'http://localhost:3001/callback'
        }
      });
    });
  });