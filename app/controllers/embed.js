var debug = require('debug'),
  express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  md = require('markdown').markdown,
  Comment = mongoose.model('Comment');

module.exports = function(app, server) {
  app.use('/', router);

};

router.get('/embed/:s', function(req, res, next) {
  Comment.find(function(err, comments) {
    if (err) return next(err);
    var style = parseInt(req.param.style);
    res.render('embed', {
      title: 'Commento.io',
      embedStyle: req.params.s,
      comments: comments.reverse(),
      md: md
    });
  });
});

router.get('/embed-settings/:embedId', function(req, res, next) {
  var style = parseInt(req.param.style);
  res.render('embed-settings', {
    title: 'Commento.io',
    embedStyle: req.params.embedId
  });
});


router.get('/embed/semantic-ui/:embedId', function(req, res, next) {
  var style = parseInt(req.param.style);
  res.render('embed-semantic-ui', {
    title: 'Commento.io',
    theme: 'Commento.io',
    title: 'Commento.io',
    title: 'Commento.io',
    embedStyle: req.params.embedId
  });
});