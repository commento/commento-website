// Example model

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var CommentSchema = new Schema({
  context: String,
  commenter: String,
  comment: String,
  upvotes: Number,
  downvotes: Number,
  favs: Number,
  datetime: { type: Date, default: Date.now },
  approved: Boolean
});

CommentSchema.virtual('date')
  .get(function() {
    return this._id.getTimestamp();
  });

mongoose.model('Comment', CommentSchema);

