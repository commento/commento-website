// Example model

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var CommenterSchema = new Schema({
  ref_context: String,
  name: String,
  email: String,
  rank: String
});

CommenterSchema.virtual('date')
  .get(function() {
    return this._id.getTimestamp();
  });

mongoose.model('Commenter', CommenterSchema);