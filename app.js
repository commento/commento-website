var http = require('http'),
  config = require('./config/config'),
  glob = require('glob'),
  debug = require('debug')("commento"),
  mongoose = require('mongoose'),
  dotenv = require('dotenv'),
  jwt = require('express-jwt'),
  swig = require('swig'),
  passport = require('passport'),
  logger = require('morgan'),
  cors = require('cors'),
  express = require('express');
var app = module.exports.app = express();
var Auth0Strategy = require('passport-auth0'),
  cookieParser = require('cookie-parser'),
  session = require('express-session'),
  bodyParser = require('body-parser');

dotenv.load();

// Configure passport
var passportConfig = function(app) {
  var strategy = new Auth0Strategy({
    domain: process.env['AUTH0_DOMAIN'],
    clientID: process.env['AUTH0_CLIENT_ID'],
    clientSecret: process.env['AUTH0_CLIENT_SECRET'],
    callbackURL: process.env['AUTH0_CALLBACK_URL'] || 'http://localhost:3000/callback'
  }, function(accessToken, refreshToken, profile, done) {
    //Some tracing info
    console.log('profile is', profile);
    //save the profile
    return done(null, profile);
  });

  passport.use(strategy);

  // you can use this section to keep a smaller payload
  passport.serializeUser(function(user, done) {
    done(null, user);
  });

  passport.deserializeUser(function(user, done) {
    done(null, user);
  });

  app.use(passport.initialize());
  app.use(passport.session());

};
passportConfig(app);

var authenticate = jwt({
  secret: new Buffer(process.env.AUTH0_CLIENT_SECRET, 'base64'),
  audience: process.env.AUTH0_CLIENT_ID
});

app.i18n = require("./lib/i18n")(app);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/secured', authenticate);
app.use(cors());

mongoose.connect(config.db);
var db = mongoose.connection;
db.on('error', function() {
  throw new Error('unable to connect to database at ' + config.db);
});

var models = glob.sync(config.root + '/app/models/*.js');
models.forEach(function(model) {
  require(model);
});

app.get('/ping', function(req, res) {
  res.send(200, {
    text: "All good. You don't need to be authenticated to call this"
  });
});

app.get('/secured/ping', function(req, res) {
  res.send(200, {
    text: "All good. You only get this message if you're authenticated"
  });
})

require('./config/express')(app, config);
var server = http.createServer(app);
var io = require('socket.io').listen(server);

server.listen(config.port);

io.on('connection', function(socket){

  var room = socket.handshake['query']['r_var'];

  var Comment = mongoose.model('Comment');
  socket.join(room);

  socket.on('comment', function(msg) {
    var comment = new Comment({
      comment: msg.comment,
      commenter: msg.commenter,
      context: msg.context
    });
    comment.save();
    debug(msg);
    io.emit('comment', msg);
  });

});

server.listen(process.env.PORT || 80);