# Commento 

A commenting system. 

## Installation


    $ git clone https://bitbucket.org/commento/commento-website
    $ cd commento-website
    $ npm install

Then place a .env file in the app root in order to load your Auth0 Secrets.

		AUTH0_CLIENT_ID=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 
		AUTH0_CLIENT_SECRET=XXXXXX-XXXXXXXXXXXXXXXXXXXXXXXXXXXXX-XXXXXXXXXXXXXXXXXXXXXXXXXXX 
		AUTH0_DOMAIN=XXXXXXXXX.auth0.com
		AUTH0_CALLBACK_URL=http://localhost:3001/callback

## Running
    
    $ npm start 


### Running with debugging on

**Commento** uses the [debug](http://npmjs.com/package/debug) module for logging messages to the console.

    $ DEBUG='commento*' npm start 

**Launch your browser in http://localhost:3001**


