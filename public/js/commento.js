$(function() {

  $('#s').bind('click', function() {
    sendMessage(); 
    return false;
  });

  $("#c").keyup(function(e) {
    // Check the keyCode and if the user pressed Enter (code = 13) 
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 13) {
      e.preventDefault();
      e.stopPropagation();
      sendMessage();
    }

  });

  function sendMessage() {
    var comment = $('#c').val();
    if(comment.length > 1){
      socket.emit('comment', {
        'comment': comment,
        'commenter': "Anonym cow",
        'context': "localhost"
      }); 
      $('#c').val('');
    }
  }

  socket.on('comment', function(msg) {

    var li = $('<div class="well comment" style="display:none;">');
    //$(li).prepend($('<span class="comment-context">').html(msg.context));
    $(li).prepend($('<p class="comment-body">').html(markdown.toHTML(msg.comment)));
    $(li).prepend($('<b>').html(msg.commenter + 'dijo:'));

    $('#comments').prepend(li);
    $(li).show();
  });
});